# Bouncing Ball Simulation

O objetivo do projeto é analisar o comportamento de uma bola, sendo seu movimento descrito de acordo com as leis da física.

# Instruções para compilação

- g++ -g -o test-ball test-ball.cpp ball.cpp -I.

Basta abrir o terminal na pasta contendo os arquivo e digitar o comando `make`. A flag `-g` serve para realizar o debug utilizando
o `gdb`.

# Descrição dos Arquivos

- ball.cpp

Contém as implementações dos métodos da classe `Ball`, os quais são utilizados para modelar o comportamneto da bola e para posterior
análise de seu movimento.

- ball.h

Contém a definição da classe de estudo do projeto(`Ball`). Possuindo, assim, os protótipos dos métodos necessários para utilizar 
a classe `Ball`  e também seus atributos.

- test-ball.cpp

Contém o módulo responsável por realizar a simulação. Esse módulo cria um objeto da classe `Ball` e analisa a posição da bola
em diferentes instantes de tempo.

- simulation.h

Módulo contém somente uma classe puramente abstrata com os dois principais métodos da simulação(métodos chamados no arquivo `test-ball-cpp`).

- Makefile

Contém os comandos de compilação.

# Saída

Seguem as 10 primeiras linhas da saída no seguinte formato:

- posição da bola eixo x posição da bola eixo y 


	0.01 -0.00877778  
	0.02 -0.0284444  
	0.03 -0.059  
	0.04 -0.100444  
	0.05 -0.152778  
	0.06 -0.216  
	0.07 -0.290111  
	0.08 -0.375111  
	0.09 -0.471  
	0.1 -0.577778  

# Diagrama de Classes

![alt text](ball.png)

# Gráfico da Simulação

![alt text](posicao.png)

# Spring Mass Simulation

O objetivo desse projeto é verificar um sistema no qual duas massas estão conectadas por uma mola e variam sua posição ao longo do tempo.
A descrição do movimento das massas conectadas à mola segue as leis da física.Ao fim do projeto, será criada uma simulação gráfica para
verificar o comportamento do sistema.

# Instruções para compilação

- g++ -g -o test-springmass test-springmass.cpp springmass.cpp -I.

Os comandos de compilação estão todos contindos no arquivo `Makefile`, sendo necessário somente abrir o terminal na pasta do projeto e
digitar o comando `make`. A flag `-g` é utilizada para realizar o debug utilizando o `gdb`.

# Descrição dos arquivos

- springmass.h

Contém as definições das classes principais da simulação(`Mass`,`Spring` e `SpringMass`). Possuindo assim os protótipos dos 
métodos necessários para utilizar essas classes, e também os atributos das mesmas.

- springmass.cpp

Contém as implementações dos métodos definidos no arquivo `springmass.h`.

- test-springmass-cpp

Contém o módulo responsável por realizar de fato a simulação. Esse módulo cria os objetos e chama a simulação para diferentes instantes
de tempo.

- simulation.h

Contém o módulo já utilizado no experimento anteriro, o qual contém somente uma classe puramente abstrata com os dois principais métodos
da simulação(esses métodos são chamados no arquivo `test-springmass-cpp`).

- Makefile

Contém os comandos de compilação.

# Diagrama de Classes

![alt text](springmass.jpg)

# Saída

Segue as 10 primeiras linhas da saída no seguinte formato:

- posicao de cada massa(x,y) energia total do sistema

	-0.495634 -0.00653366 0.494884 -0.00524239 -0.194449 0.338313 0.695199 0.891641 0.875955  
	-0.482603 -0.0260922 0.479642 -0.0209787 -0.177949 0.333145 0.68091 0.866637 0.855868  
	-0.461185 -0.0585052 0.454694 -0.0472444 -0.151105 0.324074 0.657596 0.825276 0.796583  
	-0.431935 -0.10343 0.420871 -0.0841065 -0.115101 0.31026 0.626165 0.768121 0.705714  
	-0.395697 -0.16036 0.379391 -0.131654 -0.0716299 0.290468 0.587937 0.695991 0.592709  
	-0.353605 -0.228647 0.33182 -0.189984 -0.0228062 0.26312 0.54459 0.609911 0.467093  
	-0.307062 -0.307544 0.280022 -0.259182 0.0289537 0.226371 0.498087 0.511067 0.336653  
	-0.257723 -0.396278 0.226083 -0.339296 0.0810562 0.178222 0.450584 0.400729 0.20592  
	-0.207438 -0.494126 0.172242 -0.430304 0.13086 0.116653 0.404337 0.280177 0.0752275  
	-0.158191 -0.600525 0.120788 -0.532077 0.175798 0.0397763 0.361605 0.150603 -0.0594733  

# Simulação Grafica Utilizando Octave

- Inicio da Simulacao

![alt text](simulacao1.png)


- Estágios Intermediários 

![alt text](simulacao2.png)
![alt text](simulacao3.png)
![alt text](simulacao4.png)


- Final da Simulação 

![alt text](simulacao5.png)

# Graphics

Feitas as duas simulações anteriores, o objetio é tornar as mesmas gráficas. Para isso, utilizando a biblioteca OpenGL e a GLUT, a simulação
agora gera automaticamente uma saída gráfica para modelar os movimentos tanto da classe `Ball` quanto da classe `SpringMass`.

# Dependências do Programa

Para executar a parte gráfica da simulação, foram necessárias as seguintes bibliotecas:

- OpenGl e Glut

Para instalação das bibliotecas no Ubuntu, basta digitar no terminal: `sudo apt-get install freeglut3 freeglut3-dev`.

# Instruções de compilação

Para compilar o programa, basta acessar o terminal na pasta do mesmo e digitar o comando `make`. Serão gerados 4 executáveis, referentes
as simulações gráficas e não gráficas das classes `Springmass` e `Ball`.
Para que o programa consiga utilizar as bibliotecas `OpenGL` e `Glut` as seguintes diretivas foram adicionadas:

	-lGL -lGLU -lglut   

Linha de compilação do programa `test-springmass-graphics.cpp`:

	g++ -g -I. graphics.cpp springmass.cpp test-springmass-graphics.cpp -lGL -lGLU -lglut   

# Diagrama de Classes

![alt text](graphics.jpg)

# Diagrama de Eventos

![alt text](eventsdiagram.jpg)

# Saída Gráfica

- Simulação classe `Ball` 

![alt text](ball1.png)
![alt text](ball2.png)  

- Simulação classe `Springmass` 

![alt text](springmass1.png)
![alt text](springmass2.png)
![alt text](springmass3.png)  

- Simulação classe `Springmass` contendo a energia do sistema

Snapshot no inicio da simulação e no final da simulação respectivamente

![alt text](energy1.png)
![alt text](energy2.png)



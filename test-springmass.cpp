/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

int main(int argc, char** argv)
{
  SpringMass springmass ;

  const double mass = 0.05;
  const double radius = 0.02 ;
  const double naturalLength = 0.5;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;
  Mass m3(Vector2(-.2,0.34), Vector2(), mass, radius) ;
  Mass m4(Vector2(+.7,0.9), Vector2(), mass, radius) ;

  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addMass(m3);
  springmass.addMass(m4);
  springmass.addSpring(0,1,naturalLength,0,1);
  springmass.addSpring(2,3,naturalLength,0,1);
  springmass.addSpring(0,2,naturalLength,0,1);

  const double dt = 1.0/30 ;
  for (int i = 0 ; i < 100 ; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
  }

  return 0 ;
}

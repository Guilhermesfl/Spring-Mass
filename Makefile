.PHONY: all, clean, distclean, pack, pack-solution

all: test-ball test-ball-graphics test-springmass test-springmass-graphics
test-ball : ball.cpp ball.h test-ball.cpp
test-springmass : springmass.cpp test-springmass.cpp
test-ball-graphics : graphics.cpp ball.cpp test-ball-graphics.cpp -lGL -lGLU -lglut 
test-springmass-graphics : graphics.cpp springmass.cpp test-springmass-graphics.cpp -lGL -lGLU -lglut 

CXX=g++
CXXFLAGS= -g -I.

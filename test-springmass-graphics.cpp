/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
private:
  Figure figure ;

public:
  SpringMassDrawable()
  : figure("SpringMass")
  {
    figure.addDrawable(this) ;
  }

  void draw() {
  	Vector2 v1,v2;
	Mass *m1, *m2;
  	double radius;
  	double energy = getEnergy();
	int i=0;	
	while(i <  (int)masses.size()){
		v1 = masses[i].getPosition();
		radius = masses[i].getRadius();
    	figure.drawCircle(v1.x,v1.y,radius) ;
		i++;	
	}
	i = 0;
	while(i < springs.size()){
		m1 = springs[i].getMass1();
		v1 = m1->getPosition();
		m2 = springs[i].getMass2();
		v2 = m2->getPosition();
		i++;	
		figure.drawLine(v1.x,v1.y,v2.x,v2.y,2) ;
	}
	ostringstream strs;
	strs << energy;
	string str = strs.str();
	figure.drawString(-0.9,0.7,"TotalEnergy");
	figure.drawString(-0.9,0.5, str);

  }

  void display() {
    figure.update() ;
  }

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;

  const double mass = 0.05;
  const double radius = 0.06 ;
  const double naturalLength = 0.4;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;
  Mass m3(Vector2(-.2,0.34), Vector2(), mass, radius) ;
  Mass m4(Vector2(+.7,0.9), Vector2(), mass, radius) ;

  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addMass(m3);
  springmass.addMass(m4);
  springmass.addSpring(0,1,naturalLength,-0.05,1);
  springmass.addSpring(2,3,naturalLength,-0.05,1);

  run(&springmass, 1.0/30) ;
}
